Name:           gnome-desktop-testing
Version:        2018.1
Release:        %autorelease
Summary:        GNOME test runner for installed tests

License:        LGPL-2.0-or-later
URL:            https://live.gnome.org/Initiatives/GnomeGoals/InstalledTests
Source0:        https://gitlab.gnome.org/GNOME/%{name}/-/archive/v%{version}/%{name}-v%{version}.tar.gz

BuildRequires:  pkgconfig(gio-unix-2.0)
BuildRequires:  pkgconfig(libsystemd)
# I don't see any libgsystem dependencies
#BuildRequires:  pkgconfig(libgsystem)
BuildRequires:  git automake autoconf libtool
BuildRequires: make

# https://gitlab.gnome.org/GNOME/gnome-desktop-testing/merge_requests/1
Patch0: 0001-Don-t-crash-on-unknown-command-line-options.patch
Patch1: 0001-Resolve-RHELPLAN-170235-Fix-OpenScanHub-report.patch

%description
gnome-desktop-testing-runner is a basic runner for tests that are
installed in /usr/share/installed-tests.  For more information, see
"https://wiki.gnome.org/Initiatives/GnomeGoals/InstalledTests"

%prep
%autosetup -S git_am -n %{name}-v%{version}
NOCONFIGURE=1 ./autogen.sh

%build
%configure
make %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT

%files
%doc COPYING README
%{_bindir}/gnome-desktop-testing-runner
%{_bindir}/ginsttest-runner

%changelog
%autochangelog

