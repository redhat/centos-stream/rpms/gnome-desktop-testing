#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
    rlPhaseStartTest
        rlRun "export XDG_DATA_DIRS=$PWD"
        rlRun -t "gnome-desktop-testing-runner app1" 0
    rlPhaseEnd
rlJournalEnd
